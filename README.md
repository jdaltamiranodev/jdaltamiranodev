# David Altamirano - Desarrollador Full Stack Web

¡Hola! 👋 Bienvenido a mi perfil de GitLab. Soy David Altamirano, un apasionado por la tecnología, el desarrollo web, las interfaces y la creación de experiencias interactivas. Me especializo en el desarrollo Full Stack, combinando habilidades tanto en el frontend como en el backend para crear soluciones web completas y efectivas.

## Stack Tecnológico

### Lenguajes de Programación

<div>
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg" title="HTML5" alt="HTML" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-plain-wordmark.svg"  title="CSS3" alt="CSS" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/sass/sass-original.svg" title="SASS" alt="SASS" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" title="JavaScript" alt="JavaScript" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" title="Python" alt="Python" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" title="Java" alt="Java" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/php/php-original.svg" title="PHP" alt="PHP" width="40" height="40"/>&nbsp;
</div>

### Frontend y Frameworks

<div>
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vuejs/vuejs-original.svg" title="Vue.js" alt="Vue.js" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vite/vite-original.svg" title="Vite" alt="Vite" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vitest/vitest-original.svg" title="Vitest" alt="Vitest" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/bootstrap/bootstrap-plain.svg" title="Bootstrap" alt="Bootstrap" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/tailwindcss/tailwindcss-original.svg" title="Tailwind CSS" alt="Tailwind CSS" width="40" height="40"/>&nbsp;

</div>

### Control de Versiones y Hosting

<div>
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/git/git-original.svg" title="Git" alt="Git" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/github/github-original.svg" title="GitHub" alt="GitHub" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/gitlab/gitlab-original.svg" title="GitLab" alt="GitLab" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/bitbucket/bitbucket-original.svg" title="Bitbucket" alt="Bitbucket" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/netlify/netlify-original.svg" title="Netlify" alt="Netlify" width="40" height="40"/>&nbsp;
</div>

### Bases de Datos

<div>
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/postgresql/postgresql-original.svg" title="PostgreSQL" alt="PostgreSQL" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original.svg" title="MySQL" alt="MySQL" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/sqlite/sqlite-original.svg" title="SQLite" alt="SQLite" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mongodb/mongodb-original.svg" title="MongoDB" alt="MongoDB" width="40" height="40"/>&nbsp;
</div>

### Backend y Frameworks

<div>
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/spring/spring-original.svg" title="Spring" alt="Spring" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/flask/flask-original.svg" title="Flask" alt="Flask" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/fastapi/fastapi-original.svg" title="FastAPI" alt="FastAPI" width="40" height="40"/>&nbsp;
  <img src="https://img.icons8.com/color/48/000000/django.png" title="Django" alt="Django" width="40" height="40"/>&nbsp;
</div>

### Testing

<div>
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/pytest/pytest-original.svg" title="Pytest" alt="Pytest" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/jest/jest-plain.svg" title="Jest" alt="Jest" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vitest/vitest-original.svg" title="Vitest" alt="Vitest" width="40" height="40"/>&nbsp;
</div>

### Java Build Tools

<div>
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/apache/apache-original.svg" title="Apache" alt="Apache" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/maven/maven-original.svg" title="Maven" alt="Maven" width="40" height="40"/>&nbsp;
</div>

## Proyectos Destacados

### Blog de Café

El Blog de Café es un sitio web estático que sirve como modelo de blog, diseñado para ofrecer una experiencia agradable a los amantes del café. Este proyecto lo he realizado utilizando tecnologías web como HTML5, CSS3, utiliza características como Flex Box, CSS Grid, y Normalize. Además cuenta con diseño responsivo para adaptarse a diferentes dispositivos y tamaños de pantalla.
¡Visita el proyecto! [Blog de Cafe](https://blog-cafe-jose-david-altamirano.netlify.app/)

### E-commerce Ropa

E-commerce es una web estatica de tipo catalogo de productos que consta de: una página de inicio ó catalogo donde se muestran los productos de la tienda de ropa y el usuario puede añadir productos al carrito, filtra por categorías, entre otros. Tambien incluye una página de carrito de compras donde se puede terminar la compra, vaciar el carrito, eliminar productos. La página incluye el almacenamiento de productos en Local Storage, y se puede añadir y eliminar productos del mismo en tiempo real.

¡Visita el proyecto! [E-commerce Ropa](https://tienda-ecommerce-jose-david-altamiran.netlify.app/)

## Sobre Mí

Soy David Altamirano, un desarrollador Full Stack con una pasión por construir soluciones web robustas y atractivas. Mi enfoque se centra en la creación de interfaces intuitivas y en el desarrollo de backend eficiente para proporcionar experiencias de usuario excepcionales.

¡Únete a mi viaje tecnológico! Si compartes la misma pasión por la tecnología y el desarrollo web, ¡sigámonos y colaboremos en proyectos interesantes!

[![X](https://img.shields.io/badge/X-XXXXXX?style=for-the-badge&logo=x&logoColor=white)](https://twitter.com/jdaltamiranodev)
[![LinkedIn](https://img.shields.io
